import datetime
import random

class Monitor:

    def __init__(self):
        self.servers = [
            '192.168.1.1',
            '192.168.1.2',
            '192.168.1.120',
            '192.168.100.1',
            'google.com'
        ]

    def log(server, status):
        print "%s|%s|%s" % (datetime.datetime.now().isoformat(), server, status)

    def ping(server):
        try:
            response=urllib2.urlopen('http://',timeout=1)
            return True
        except urllib2.URLError as err: pass
        return False
    
    def test(server):
        self.log(server, ping(server))
    
    def loop():
        for server in self.servers:
            self.ping(server)

    