#!/usr/bin/env python
from threading import Thread
import subprocess
from Queue import Queue
import datetime
import time
import sys
import fcntl

def ping(ip):
    ret = subprocess.call("ping -c 1 %s" % ip,
        shell=True,
        stdout=open('/dev/null', 'w'),
        stderr=subprocess.STDOUT)
    return bool(ret == 0)

def log(ip, alive):
    fcntl.flock(sys.stdout, fcntl.LOCK_EX)
    print "%s|%s|%s" % (
        datetime.datetime.now().isoformat(),
        ip,
        alive,
    )
    sys.stdout.flush()
    fcntl.flock(sys.stdout, fcntl.LOCK_UN)

def pinger(i, q):
    """Pings subnet"""
    while True:
        ip = q.get()
        # print "Thread %s: Pinging %s" % (i, ip)
        log(ip, ping(ip))
        q.task_done()

queue = Queue()

def setup():
    num_threads = 4
    #wraps system ping command
    #Spawn thread pool
    for i in range(num_threads):
        worker = Thread(target=pinger, args=(i, queue))
        worker.setDaemon(True)
        worker.start()

def test():
    ips = [
        "192.168.1.1", "192.168.1.2", "192.168.1.3",
        "192.168.1.120", "192.168.100.1", "216.58.216.142",
    ]
    #Place work in queue
    for ip in ips:
        queue.put(ip)
    #Wait until worker threads are done to exit    
    # queue.join()

def loop():
    while True:
        starttime = datetime.datetime.now()
        test()
        time.sleep(60)

if __name__ == '__main__':
    setup()
    loop()